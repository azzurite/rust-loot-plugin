using Rust;
using ConVar;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Reflection;
using UnityEngine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Oxide.Core.Configuration;
using Random = System.Random;
using Oxide.Core;
using Oxide.Core.Plugins;
using Facepunch;
using Facepunch.Extend;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using System.Drawing;
using ConVar;
using Oxide.Core;
using Oxide.Core.Configuration;
using Oxide.Core.Libraries.Covalence;
using Rust.Ai.HTN.Bear.Reasoners;
using UnityEngine;
using VLB;

namespace Oxide.Plugins
{
	[Info("AllLoot", "Azzu", "1.0.0")]
	[Description("Always Spawns all loot")]
	public class AllLoot : RustPlugin
	{
		Dictionary<string, string> lootnames = new Dictionary<string, string>()
		{
			{"crate_normal", "green"},
			{"crate_normal_2", "yellow"},
		};
		
		Dictionary<string, string> entnames = new Dictionary<string, string>()
		{
			{"assets/bundled/prefabs/static/recycler_static.prefab", "+++ Recycler +++"},
			{"assets/bundled/prefabs/static/small_refinery_static.prefab", "=== Refinery ==="},
			{"assets/bundled/prefabs/static/repairbench_static.prefab", "||| Repair Bench |||"},
		};
		
		string getLootName(SpawnGroup.SpawnEntry r) {
			var name = r.prefab.Get().name;
			string val;
			if (lootnames.TryGetValue(name, out val)) {
				return val;
			} else {
				return name;
			}
		}

		float getRadSize(TriggerRadiation entity)
		{
			var collider = entity.GetComponent<SphereCollider>();
			return collider.radius * entity.transform.localScale.Max();
		}

		void showRads(BasePlayer player)
		{
			var list = UnityEngine.Object.FindObjectsOfType<TriggerRadiation>();
			foreach (var entity in list) {
				var pos = entity.transform.position;
				var distance = Vector3.Distance(player.transform.position, pos);
				if (distance < 200) {
					player.SendConsoleCommand("ddraw.text", 5, UnityEngine.Color.green, pos, "Rad needed: " + entity.GetRadiationAmount(),ToString());
				}
				player.SendConsoleCommand("ddraw.sphere", 5, UnityEngine.Color.green, pos, getRadSize(entity));
			}
		}

		void OnPlayerChat(BasePlayer player, string message, Chat.ChatChannel channel)
		{
			showRads(player);
			// var list = GameManager.server.FindPrefab("assets/bundled/prefabs/static/recycler_static.prefab");
			var list = UnityEngine.Object.FindObjectsOfType<BaseEntity>();
			foreach (var entity in list) {
				if (entnames.ContainsKey(entity.name)) {
					 showText(player, entity.transform.position, entnames[entity.name], UnityEngine.Color.red);
				}
			}

			var color = ColorTranslator.FromHtml("#FFF");
			foreach (var iSpawnGroup in SingletonComponent<SpawnHandler>.Instance.SpawnGroups) {
				var spawnGroup = iSpawnGroup as SpawnGroup;
				if (spawnGroup == null) {
					continue;
				}
				
				var sb = new StringBuilder();
				
				foreach (var p in spawnGroup.prefabs) {
					sb.Append(getLootName(p));
					sb.Append(" ");
					sb.Append(p.weight.ToString());
					sb.Append("\n");
				}
				
				var spawns = spawnGroup.spawnPoints;
				foreach (var spawn in spawns) {
					Vector3 pos;
					Quaternion rot;
					spawn.GetLocation(out pos, out rot);
					showText(player, pos, sb.ToString());
				}
				var spawnsAmount = spawns.Length;
				if (spawnsAmount > spawnGroup.maxPopulation) {
					spawnGroup.maxPopulation = spawnsAmount; 
					spawnGroup.numToSpawnPerTickMin = spawnsAmount;
					spawnGroup.numToSpawnPerTickMax = spawnsAmount;
				}
			}
		}
		
		void showText(BasePlayer player, Vector3 pos, string text)
		{
			showText(player, pos, text, UnityEngine.Color.white);
		}
		
		void showText(BasePlayer player, Vector3 pos, string text, UnityEngine.Color color)
		{
			var distance = Vector3.Distance(player.transform.position, pos);
			if (distance < 60) {
				player.SendConsoleCommand("ddraw.text", 5, color, pos, text);
			}
		}
	}
}
